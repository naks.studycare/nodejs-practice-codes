const http = require('http')
const https = require('https')
const fs = require('fs')

const url = "https://jsonplaceholder.typicode.com/posts"
const server = http.createServer((req,serverres)=>{
    if(req.method === 'GET' && req.url === '/show')
    {
        https.get(url,(httpres)=>{
            httpres.on("data",(data)=>{
                httpres.setEncoding('UTF-8')
               // serverres.write(data)   // showing all data in browser
                console.log(data)
            })

            httpres.on('end',()=>{
                serverres.end()
                console.log('its over. Thank you')
            })
        })
    }
}).listen(3001,()=>console.log('server is running'))