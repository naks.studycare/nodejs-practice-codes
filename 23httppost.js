const http = require('http')
const https = require('https')
const fs = require('fs')

const server = http.createServer((req,res)=>{
var body = ''
    if(req.method==='GET'){
        res.writeHead(200,{'Content-Type':'text/html'})
        fs.readFile('./23.html','UTF-8',(err,data)=>{
            if(err) return err
            res.write(data)
            res.end()
        })
    }

    else if(req.method === 'POST'){
        req.on('data',(data)=>{
            body+=data
        })

        req.on('end',()=>{
            res.writeHead(200,{'Content-Type':'text/html'})
            res.write(body,'UTF-8',()=>{
                res.end()
            })
        })
    }

    else{
        res.writeHead(404,{'Content-Type':'text/plain'})
            res.end('404 can not find your request page')
    }

    
}).listen(3002,()=>console.log('server is running'))