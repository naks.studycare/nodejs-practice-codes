const express = require('express')

const app = express()

// creating custom middleware
function ourMiddleWare(req,res,next){
    console.log('i am middleware'+ req.url)
    next()
}

//using middleware    // another way of middleware using is as a parametere of get
app.use(ourMiddleWare)

app.get('/',(req,res)=>{
    res.send('<h1>home page</h1>')
})

app.listen(2001,()=>console.log('running on port 2001'))