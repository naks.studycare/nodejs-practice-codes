const http = require ('http')
const fs = require ('fs')

const server = http.createServer((req,res)=>{
    if(req.url == '/')
    {
        fs.readFile('./index.html','UTF-8',(err,data)=>{
            if(err) return err
            res.writeHead(200,{'Content-Type':'text/html'})
            res.end(data)
        })
    }

    else{
        res.writeHead(404,{'Content-Type':'text/plain'})
        res.end('404 page not found')
    }
}).listen(3000,()=> console.log('server is running on port 3000'))