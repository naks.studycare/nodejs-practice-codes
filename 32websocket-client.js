const ws = new WebSocket('ws:localhost:3232')


//.. showing message all clients
ws.onmessage = (payload)=>{
    showmessage(payload.data)
}
//...................................

ws.onopen = ()=>{
    showtitle('Messanger Connection established')
}

ws.onclose = ()=>{
    showtitle('connection close')
}

function showtitle(title){
document.querySelector('h1').innerHTML=title
}

var i=0
function showmessage(message){ 
       i++
      
    let h1 = document.createElement('h1')
    h1.innerHTML=message

        document.querySelector('div.message').appendChild(h1)

        if(i%2 ==0 )
        {
            h1.style.color="blue"
        }
        else{
            h1.style.color="green"
        }
}


document.forms[0].onsubmit = ()=>{
    let message = document.getElementById('message')
    console.log(message.value)

    ws.send(message.value)  // send value to server 
}