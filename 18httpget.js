const https = require('https')
const fs = require('fs')


var url = "https://jsonplaceholder.typicode.com/posts"

https.get(url,res=>{

    res.setEncoding("utf8")
    var body=''

    res.on('data', data=>{
        body+=data
    })


    res.on('end',()=>{

        // write file in data.josn

        // fs.writeFile('data.json',body,"utf8",(err)=>{
        //         if(err) return err
        //         console.log('all data pushed on data.json')
        // })

        // showing individually

        body=JSON.parse(body)

        console.log(`${body[0].title}`)
        console.log(body)
    })

})